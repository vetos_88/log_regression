

import pandas as pd
from math import log, exp
from sklearn.metrics import roc_auc_score
from sklearn.linear_model import LogisticRegression
import numpy as np
from functools import partial




def error(x, y, ws, c):
    # print(ws)
    # print(x)
    return log(1+exp(-y*(x[0]*ws[0]+x[1]*ws[1]))) + 1/2*c*(ws[0]**2+ws[1]**2)


def sum_error(x, y, ws, c):
    sum_err = 0
    for x_i, y_i in zip(x, y):
        sum_err += error(x_i, y_i, ws, c)
    return (1.0 / len(x)) * sum_err


def grad_st(x, y, ws, c):
    grad_step = []
    for i in range(len(ws)):
        step = x[i] * y * (1 - 1 / (1 + exp(-y * (x[0] * ws[0] + x[1] * ws[1])))) - c * ws[i]
        grad_step.append(step)

    return np.array(grad_step)


def grad(x, y, ws, c):
    sum_steps = 0
    for x_i, y_i in zip(x, y):
        # print(x_i, y_i)
        sum_steps += grad_st(x_i, y_i, ws, c)

    return np.array((1.0 / len(x)) * sum_steps)


def decent(error_fn, grad_fn, k, c, numiter, ws = np.array([0, 0])):
    # print("Ws " + str(ws))
    value = error_fn(ws, c)
    for i in range(numiter):
        grad = grad_fn(ws, c)
        next_ws = ws + k*grad
        next_value = error_fn(next_ws, c)
        if abs(value - next_value) < .000001:
            print("sum iter " + str(i))
            return ws
        else:
            ws, value = next_ws, next_value
    print("sum iter " + str(i))
    return ws


def predict(x, ws):
    pred = []
    for x_i in x:
        pr_i = 1 / (1 + exp(-(x_i[0] * ws[0] + x_i[1] * ws[1])))
        pred.append(pr_i)

    return pred

df = pd.read_csv('data-logistic.csv', header=None)
X = df[[1, 2]].values
y = df[0].values

error_fn = partial(sum_error,X,y)
grad_fn = partial(grad,X,y)

# clf = LogisticRegression()
# clf.fit(X,y)
ws = np.array([0, 0])
print("Ws = " + str(ws))
ws1 = decent(error_fn,grad_fn,0.1,0,10000,ws=ws)   # без регуляризации
ws2 = decent(error_fn,grad_fn,0.1,10,10000,ws=ws)  # с регуляризацией
roc_ws1 = roc_auc_score(y,predict(X,ws1))
roc_ws2 = roc_auc_score(y,predict(X,ws2))
print("AUC-ROC without reg " + str(roc_ws1))
print("AUC-ROC with reg " + str(roc_ws2))


ws = np.array([1, 1])
print("Ws = " + str(ws))
ws1 = decent(error_fn,grad_fn,0.1,0,10000, ws = ws)   # без регуляризации
ws2 = decent(error_fn,grad_fn,0.1,10,10000, ws = ws)
roc_ws1 = roc_auc_score(y,predict(X,ws1))
roc_ws2 = roc_auc_score(y,predict(X,ws2))
print("AUC-ROC without reg " + str(roc_ws1))
print("AUC-ROC with reg " + str(roc_ws2))

ws = np.array([23, 23])                                 # увеличиваеться время сходимости
print("Ws =" + str(ws))
ws1 = decent(error_fn,grad_fn,0.1,0,10000, ws = ws)   # без регуляризации
ws2 = decent(error_fn,grad_fn,0.1,10,10000, ws = ws)
roc_ws1 = roc_auc_score(y,predict(X,ws1))
roc_ws2 = roc_auc_score(y,predict(X,ws2))
print("AUC-ROC without reg " + str(roc_ws1))   # ухудшаеться AUC-ROC
print("AUC-ROC with reg " + str(roc_ws2))


print()
print("Var step 0.1 - 0.00001")
ws = np.array([0, 0])
print("Ws = " + str(ws))
for i in range(1, 2):
    k = 1/10**i
    print("Step = " + str(k))
    ws1 = decent(error_fn,grad_fn,k,0,10000,ws=ws)
    ws2 = decent(error_fn,grad_fn,k,10,10000,ws=ws)
    roc_ws1 = roc_auc_score(y,predict(X,ws1))
    roc_ws2 = roc_auc_score(y,predict(X,ws2))
    print("AUC-ROC without reg " + str(roc_ws1))
    print("AUC-ROC with reg " + str(roc_ws2))

# при уменьшении шага увелисиваеться чичло итераций зато улучшаеться AUC-ROC

print()
print("Var step 0.1 - 0.19")
for i in range(10, 20):
    k = i/100
    print("Step = " + str(k))
    ws1 = decent(error_fn,grad_fn,k,0,10000,ws=ws)
    ws2 = decent(error_fn,grad_fn,k,10,10000,ws=ws)
    roc_ws1 = roc_auc_score(y,predict(X,ws1))
    roc_ws2 = roc_auc_score(y,predict(X,ws2))
    print("AUC-ROC without reg " + str(roc_ws1))
    print("AUC-ROC with reg " + str(roc_ws2))

# с увеличением шага регрессия с регуляризацией перестает сходиться и качесво ухудшаеться



